<?php

/**
 * Created by PhpStorm.
 * User: DiD
 * Date: 13/09/2017
 * Time: 20:58
 */

$oscillatorTypes = array(
        'sine', 'square', 'sawtooth', 'triangle'
);
$notes = array(
    'do' => array('32.70', '65.41', '130.81', '261.63', '523.25', '1046.50', '2093.00', '4186.01'),
    'do#' => array('34.65', '69.30', '138.59', '277.18', '554.37', '1108.73', '2217.46', '4434.92'),
    'ré' => array('36.71', '73.42', '146.83', '293.66', '587.33', '1174.66', '2349.32', '4698.64'),
    'ré#' => array('38.89', '77.78', '155.56', '311.13', '622.25', '1244.51', '2489.02', '4978.03'),
    'mi' => array('41.20', '82.41', '164.81', '329.63', '659.26', '1318.51', '2637.02', '5274.04'),
    'fa' => array('43.65', '87.31', '174.61', '349.23', '698.46', '1396.91', '2793.83', '5587.65'),
    'fa#' => array('46.25', '92.50', '185.00', '369.99', '739.99', '1479.98', '2959.96', '5919.91'),
    'sol' => array('49.00', '98.00', '196.00', '392.00', '783.99', '1567.98', '3135.96', '6271.93'),
    'sol#' => array('51.91', '103.83', '207.65', '415.30', '830.61', '1661.22', '3322.44', '6644.88'),
    'la' => array('55.00', '110.00', '220.00', '440.00', '880.00', '1760.00', '3520.00', '7040.00'),
    'la#' => array('58.27', '116.54', '233.08', '466.16', '932.33', '1864.66', '3729.31', '7458.62'),
    'si' => array('61.74', '123.47', '246.94', '493.88', '987.77', '1975.53', '3951.07', '7902.13')
);

$octaveCount = count($notes['do'])-1;
?>
<html>
<head>
    <title>Sounds</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link type="text/css" href="main.css" rel="stylesheet">
</head>
<body>
<main class="row">
    <div class="col">
        <h2 class="row">
            <span>Choix de la fréquence | </span>
            <input type="number" id="frequency-input-text"/>Hz
        </h2>

        <input type="range" min="30" max="8000" value="10" step="10" id="frequency-input-range"/>
        <div>
            Afficher les fréquences dans les boutons<input id="show-btn-frequency-checkbox" type="checkbox" />
        </div>
        <table id="frequencies-buttons-table">
            <tr>
                <th>Num. Octave</th>
                <?php
                foreach ($notes as $noteName => $frequencies) {
                    ?>
                    <th><?= $noteName ?></th>
                    <?php
                }
                ?>
            </tr>
            <?php
            for ($octave = 0; $octave < 8; $octave++) {
                ?>
                <tr>
                    <td>
                        Octave <?= $octave ?>
                    </td>
                    <?php
                    foreach ($notes as $noteName => $frequencies) {
                        ?>
                        <td>
                            <div class="btn" data-frequency="<?= $frequencies[$octave] ?>">
                                <span class="button-note-name"><?= $noteName . ' ' . $octave; ?></span>
                                <span class="optional-frequency-text"><?= '(' . $frequencies[$octave] . 'Hz)' ?></span>
                            </div>
                        </td>
                        <?php
                    }
                    ?>
                </tr>
                <?php
            }
            ?>
        </table>
    </div>
    <div class="col note-informations">
        <h2>Note actuelle</h2>

        <h3 id="note-text"></h3>
        <div id="oscillator-types">
            <h4>Type d'oscillation</h4>
            <div>
                <?php
                foreach($oscillatorTypes as $type)
                {
                    ?>
                    <span class="btn"><?= $type ?></span>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="row actions">
            <span class="btn" id="play-button">Jouer</span>
            <span class="btn" id="stop-button">Stop</span>
        </div>
        <div class="row">
            <h3>Volume</h3>
            <!--
            <input type="range" id="volume-range" value="100" min="0" max="100"/>
            -->
        </div>
    </div>
    <div class="col">
        <h2>Visualisation</h2>
        <canvas height="300" width="300"></canvas>
    </div>
    <div class="col">
        <h2>Entrainement</h2>
        <div class="flex-1">
            <div>
                Plein écran (WIP) :
                <input type="checkbox" disabled="disabled"/>
            </div>
            <div>
                Octave minimale :
                <input type="number" min="0" value="0" max="<?= $octaveCount ?>" id="training-suboctave"/>
            </div>
            <div>
                Octave maximale :
                <input type="number" min="0" value="<?= $octaveCount; ?>" max="<?= $octaveCount; ?>" id="training-suroctave"/>
            </div>
            <div>
                Temps avant changement :
                <input type="number" min="100" value="2000" max="999999" id="time-before-change"/>ms
            </div>
            <div>
                Etape
                <span id="current-serie-step">1</span> sur
                <input type="number" min="2" value="10" max="999" id="series-number"/>
            </div>
        </div>
        <div class="row actions">
            <span class="btn" id="start-training-button">Démarrer</span>
            <span class="btn" id="stop-training-button">Arrêter</span>
        </div>
    </div>
    <div class="col">
        <h2>Piano avec clavier</h2>
        <div class="flex-1">
            A-Z-E-R-T-Y-U
            <br/>
            Q-S-D-F-G-H-J
            <br/>
            <-W-X-C-V-B-N
            <br/>
            Octave de base : <input id="octave-input" type="number" value="3" min="0" max="7"/>
        </div>
        <div class="row actions">
            <span class="btn" id=""><s>Configurer les touches</s> (WIP)</span>
        </div>
    </div>
</main>

<script type="application/javascript" src="jquery.js"></script>
<script type="application/javascript" src="SoundContext.js"></script>
<script type="application/javascript">
    var soundsContext = new SoundsContext('play-button', 'stop-button', 'frequency-input', 'note-text');
</script>
</body>
</html>