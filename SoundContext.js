/**
 * Created by DiD on 13/09/2017.
 */

class SoundsContext {
    constructor(playId, stopId, frequencyId, noteTextId) {

        //usefull variables
        this.notesFrequencies = {
            'do': ['32.70', '65.41', '130.81', '261.63', '523.25', '1046.50', '2093.00', '4186.01'],
            'do#': ['34.65', '69.30', '138.59', '277.18', '554.37', '1108.73', '2217.46', '4434.92'],
            'ré': ['36.71', '73.42', '146.83', '293.66', '587.33', '1174.66', '2349.32', '4698.64'],
            'ré#': ['38.89', '77.78', '155.56', '311.13', '622.25', '1244.51', '2489.02', '4978.03'],
            'mi': ['41.20', '82.41', '164.81', '329.63', '659.26', '1318.51', '2637.02', '5274.04'],
            'fa': ['43.65', '87.31', '174.61', '349.23', '698.46', '1396.91', '2793.83', '5587.65'],
            'fa#': ['46.25', '92.50', '185.00', '369.99', '739.99', '1479.98', '2959.96', '5919.91'],
            'sol': ['49.00', '98.00', '196.00', '392.00', '783.99', '1567.98', '3135.96', '6271.93'],
            'sol#': ['51.91', '103.83', '207.65', '415.30', '830.61', '1661.22', '3322.44', '6644.88'],
            'la': ['55.00', '110.00', '220.00', '440.00', '880.00', '1760.00', '3520.00', '7040.00'],
            'la#': ['58.27', '116.54', '233.08', '466.16', '932.33', '1864.66', '3729.31', '7458.62'],
            'si': ['61.74', '123.47', '246.94', '493.88', '987.77', '1975.53', '3951.07', '7902.13']
        };
        this.keybordKeys = [
            [65, 90, 69, 82, 84, 89, 85, 73, 79, 80],
            [81, 83, 68, 70, 71, 72, 74, 75, 76, 77],
            [226, 87, 88, 67, 86, 66, 78, 188, 190, 191, 223]
        ];
        this.lastKeyCode = 0;
        this.tone = 'do';
        this.defaultKeyboardOctave = 3;

        //training
        this.trainingTimeout = null;
        this.timeBeforeChangeStep = 2000;
        this.seriesNumber = 10;
        this.currentSerieStep = 0;

        //html tags
        this.playInput = document.getElementById(playId);
        this.stopInput = document.getElementById(stopId);
        this.frequencyInputRange = document.getElementById(frequencyId + '-range');
        this.frequencyInputText = document.getElementById(frequencyId + '-text');
        this.noteTextTag = document.getElementById(noteTextId);
        this.startTrainingButton = document.getElementById('start-training-button');
        this.stopTrainingButton = document.getElementById('stop-training-button');
        this.suroctaveInput = document.getElementById('training-suroctave');
        this.suboctaveInput = document.getElementById('training-suboctave');
        this.timeBeforeChangeStepInput = document.getElementById('time-before-change');
        this.seriesNumberInput = document.getElementById('series-number');
        this.currentSerieStepText = document.getElementById('current-serie-step');
        this.btnFrequencyCheckbox = document.getElementById('show-btn-frequency-checkbox');
        this.oscillatorTypesTag = document.getElementById('oscillator-types');
        this.octaveInput = document.getElementById('octave-input');
        this.volumeRange = document.getElementById('volume-range');

        this.isOscillatorReady();

        this.initEvents();
    }

    isOscillatorReady() {
        if(!this.context || !this.osc) {
            if (confirm("Une entrée utilisateur est maintenant nécessaire pour créer l'oscilateur")) {
                return this.createOscillator();
            } else {
                return false;
            }
        }
        return true;
    }

    createOscillator() {
        this.context = new (window.AudioContext || window.webkitAudioContext)();
        this.osc = this.context.createOscillator();
        if (!this.context || !this.osc) {
            return false;
        }
        this.updateFrequency(440);
        this.frequencyInputRange.value = this.osc.frequency.value;
        this.osc.start();
        this.gainNode = this.context.createGain();
        return true;
    }

    initEvents() {
        if (this.volumeRange) {
            this.volumeRange.addEventListener('change', function () {
                this.updateVolume(this.volumeRange.value);
            }.bind(this));
            this.volumeRange.addEventListener('input', function () {
                this.updateVolume(this.volumeRange.value);
            }.bind(this));
        }

        if (this.frequencyInputRange) {
            this.frequencyInputRange.addEventListener('change', function () {
                this.updateFrequency(this.frequencyInputRange.value);
            }.bind(this));
            this.frequencyInputRange.addEventListener('input', function () {
                this.updateFrequency(this.frequencyInputRange.value);
            }.bind(this));
        }

        if (this.frequencyInputText) {
            this.frequencyInputText.addEventListener('change', function () {
                this.updateFrequency(this.frequencyInputText.value);
            }.bind(this));
            this.frequencyInputText.addEventListener('input', function () {
                this.updateFrequency(this.frequencyInputText.value);
            }.bind(this));
        }

        if (this.btnFrequencyCheckbox) {
            this.btnFrequencyCheckbox.addEventListener('change', function () {
                this.showButtonsFrequency($(this.btnFrequencyCheckbox).is(':checked'));
            }.bind(this));
        }

        if (this.playInput) {
            this.playInput.addEventListener('click', function () {
                this.play();
            }.bind(this));
        }

        if (this.stopInput) {
            this.stopInput.addEventListener('click', function () {
                this.stop();
            }.bind(this));
        }

        if (this.startTrainingButton) {
            this.startTrainingButton.addEventListener('click', function () {
                this.startTraining();
            }.bind(this));
        }

        if (this.stopTrainingButton) {
            this.stopTrainingButton.addEventListener('click', function () {
                this.stopTraining();
            }.bind(this));
        }

        if (this.oscillatorTypesTag) {
            $(this.oscillatorTypesTag).find('span').first().addClass('selected-button');
            this.oscillatorTypesTag.addEventListener('click', function (e) {
                if ('SPAN' === e.target.nodeName) {
                    this.changeOscillatorType(e.target.textContent);
                    $(this.oscillatorTypesTag).find('span').removeClass('selected-button');
                    $(e.target).addClass('selected-button');
                }
            }.bind(this));
        }

        if (this.timeBeforeChangeStepInput) {
            this.timeBeforeChangeStepInput.addEventListener('change', function () {
                if (this.timeBeforeChangeStepInput.value > 49 && this.seriesNumberInput.value < 1000000)
                    this.timeBeforeChangeStep = this.timeBeforeChangeInput.value;
            }.bind(this));
            this.timeBeforeChangeStepInput.addEventListener('input', function () {
                if (this.timeBeforeChangeStepInput.value > 49 && this.seriesNumberInput.value < 1000000)
                    this.timeBeforeChangeStep = this.timeBeforeChangeStepInput.value;
            }.bind(this));
        }

        if (this.seriesNumberInput) {
            this.seriesNumberInput.addEventListener('change', function () {
                if (this.seriesNumberInput.value > 1 && this.seriesNumberInput.value < 1000)
                    this.seriesNumber = this.seriesNumberInput.value;
            }.bind(this));
            this.seriesNumberInput.addEventListener('input', function () {
                if (this.seriesNumberInput.value > 1 && this.seriesNumberInput.value < 1000)
                    this.seriesNumber = this.seriesNumberInput.value;
            }.bind(this));
        }

        if (this.octaveInput) {
            this.octaveInput.addEventListener('change', function () {
                let value = $(this.octaveInput).val();
                if (value == parseInt(value, 10) && value >= 0 && value <= 7) {
                    this.defaultKeyboardOctave = value;
                } else {
                    $(this.octaveInput).val(this.defaultKeyboardOctave);
                    $(this.octaveInput).blur();
                }
            }.bind(this));
        }

        $('.btn')
            .click(function () {
                if ($(this).attr('data-frequency')) {
                    soundsContext.updateFrequency($(this).attr('data-frequency'));
                }
            });

        $(window)
            .keydown(function (event) {
                this.playNoteFromKey(event.keyCode);
            }.bind(this))
            .keyup(function (event) {
                this.stopNoteFromKey(event.keyCode);
            }.bind(this));
    }

    changeOscillatorType(type) {
        if(!this.isOscillatorReady())
            return;
        this.osc.type = type;
    }

    play() {
        if(!this.isOscillatorReady())
            return;
        this.osc.connect(this.context.destination);
        this.gainNode.connect(this.context.destination);
    }

    stop() {
        if(!this.isOscillatorReady())
            return;
        this.osc.disconnect();
    }

    updateFrequency(newValue) {
        if(!this.isOscillatorReady())
            return;

        if (isNaN(newValue)) {
            console.log('Valeur de fréquence impossible : ', newValue);
        }

        newValue = (newValue < 30) ? 30 : newValue;
        newValue = (newValue > 8000) ? 8000 : newValue;

        if (this.frequencyInputRange)
            this.frequencyInputRange.value = newValue;
        if (this.frequencyInputText)
            this.frequencyInputText.value = newValue;

        this.osc.frequency.value = newValue;

        let note = this.getNearestNote(this.osc.frequency.value);
        if (note) {
            this.printNeareastNoteInformations(note);
            this.drawColor(note);
        }

    }

    showButtonsFrequency(showIt = false) {
        if (showIt) {
            $('.optional-frequency-text').show();
        } else {
            $('.optional-frequency-text').hide();
        }
    }

    startTraining() {
        this.nextTrainingStep();
        this.play();
        this.trainingTimeout = setInterval(function () {
            this.nextTrainingStep();
        }.bind(this), this.timeBeforeChangeStep)
    }

    nextTrainingStep() {
        this.currentSerieStep++;
        if (this.currentSerieStep > this.seriesNumber) {
            this.stopTraining();
            return;
        }
        if (this.currentSerieStepText)
            this.currentSerieStepText.innerHTML = this.currentSerieStep;
        this.setRandomFrequency();
    }

    stopTraining() {
        this.stop();
        this.currentSerieStep = 0;
        if (this.currentSerieStepText)
            this.currentSerieStepText.innerHTML = 1;
        if (this.trainingTimeout)
            window.clearInterval(this.trainingTimeout);
        this.trainingTimeout = null;
    }

    setRandomFrequency() {
        this.updateFrequency(this.getRandomNoteFrequency());
    }

    getRandomNoteFrequency() {
        let subOctave = 0,
            surOctave = this.notesFrequencies[this.tone].length,
            keys = Object.keys(this.notesFrequencies),
            noteFrequencies = this.notesFrequencies[keys[keys.length * Math.random() << 0]];

        if (this.suroctaveInput && this.suroctaveInput.value <= surOctave && this.suroctaveInput.value >= subOctave)
            surOctave = this.suroctaveInput.value;
        if (this.suboctaveInput && this.suboctaveInput.value <= surOctave && this.suboctaveInput.value >= subOctave)
            subOctave = this.suboctaveInput.value;

        if (this.suboctaveInput.value > surOctave) {
            this.suboctaveInput.value = surOctave;
        }

        surOctave++;

        let randomOctave = parseInt(subOctave) + Math.floor(Math.random() * (surOctave - subOctave));
        return noteFrequencies[randomOctave];
    }

    drawColor(note) {
        let color = {r: 255, g: 0, b: 0},
            c = $('canvas')[0],
            ctx = c.getContext("2d"),
            octave = 0;

        for (let index in this.notesFrequencies[this.tone]) {
            if (note.frequency < this.notesFrequencies[this.tone][index]) {
                break;
            }
            octave++;
        }

        let subTon = this.notesFrequencies[this.tone][octave - 1],
            surTon = this.notesFrequencies[this.tone][octave] ? this.notesFrequencies[this.tone][octave] : '8000',
            gap = surTon - subTon,
            pourcentageInOctave = ((note.frequency - subTon) / gap).toFixed(6),
            step = (pourcentageInOctave * 100 / 16.67),
            stepPourcentage = (step - Math.floor(step)).toFixed(2);

        if (step < 1) {
            color.r = 255;
            color.g = (stepPourcentage * 255).toFixed(0);
            color.b = 0;
        } else if (step < 2) {
            color.r = 255 - (stepPourcentage * 255).toFixed(0);
            color.g = 255;
            color.b = 0;
        } else if (step < 3) {
            color.r = 0;
            color.g = 255;
            color.b = (stepPourcentage * 255).toFixed(0);
        } else if (step < 4) {
            color.r = 0;
            color.g = 255 - (stepPourcentage * 255).toFixed(0);
            color.b = 255;
        } else if (step < 5) {
            color.r = (stepPourcentage * 255).toFixed(0);
            color.g = 0;
            color.b = 255;
        } else if (step < 6) {
            color.r = 255;
            color.g = 0;
            color.b = 255 - (stepPourcentage * 255).toFixed(0);
        }

        ctx.beginPath();
        ctx.rect(0, 0, 300, 300);
        ctx.fillStyle = 'rgb(' + parseInt(color.r) + ', ' + parseInt(color.g) + ', ' + parseInt(color.b) + ')';
        ctx.fill();
        ctx.font = "50px Roboto";
        ctx.textAlign = "center";
        ctx.fillStyle = 'white';
        ctx.fillText(note.name, 152, 122);
        ctx.fillStyle = 'black';
        ctx.fillText(note.name, 150, 120);
    }

    getNearestNote(frequency) {
        let note = {
            absoluteDifferenceWithAbsolute: 8000,
            differenceWithAbsolute: 8000,
            name: '',
            octave: 0,
            absoluteNoteFrequency: 0,
            frequency: frequency
        };

        for (let noteName in this.notesFrequencies) {
            let frequencies = this.notesFrequencies[noteName];
            for (let index in frequencies) {
                let difference = frequency - frequencies[index],
                    absoluteDifference = Math.abs(difference);
                if (absoluteDifference < note.absoluteDifferenceWithAbsolute) {
                    note.absoluteDifferenceWithAbsolute = absoluteDifference;
                    note.differenceWithAbsolute = difference.toFixed(2);
                    note.name = noteName;
                    note.octave = index;
                    note.absoluteNoteFrequency = frequencies[index];
                }
            }
        }
        if (note.differenceWithAbsolute === 8000) {
            note = null;
        }
        return note;
    }

    printNeareastNoteInformations(note) {
        if (note && this.notesFrequencies[note.name]) {
            let differenceText = 'Fréquence absolue';
            if (note.differenceWithAbsolute != 0) {
                differenceText = ' Différence : ' + note.differenceWithAbsolute + 'Hz';
            }
            this.noteTextTag.innerHTML = '<span class="real-name">' + note.name + '</span><br/>Octave ' + note.octave + ' <br/> ' + note.absoluteNoteFrequency + 'Hz <br/>' + differenceText;
            $('#frequencies-buttons-table .btn')
                .removeClass('selected-button')
                .each(function () {
                    if ($(this).find('.button-note-name').html() == note.name + ' ' + note.octave) {
                        $(this).addClass('selected-button');
                    } else if ($(this).hasClass('selected-button')) {
                        $(this).removeClass('selected-button');
                    }
                });
        }
    }

    playNoteFromKey(keyCode) {
        if (this.lastKeyCode === keyCode)
            return;
        for (let line in this.keybordKeys) {
            if (this.keybordKeys.hasOwnProperty(line)) {
                for (let index in this.keybordKeys[line]) {
                    if (this.keybordKeys[line].hasOwnProperty(index) && keyCode === this.keybordKeys[line][index]) {
                        let octave = parseInt(this.defaultKeyboardOctave),
                            noteNames = {0: 'do', 1: 'ré', 2: 'mi', 3: 'fa', 4: 'sol', 5: 'la', 6: 'si'};

                        octave += parseInt(line);

                        let frequencies = this.notesFrequencies[noteNames[index]];
                        if (frequencies) {
                            let frequency = frequencies[octave];
                            if (frequency) {
                                this.lastKeyCode = keyCode;
                                this.stop();
                                this.updateFrequency(frequency);
                                this.play();
                            }
                        }
                    }
                }
            }
        }
    }

    stopNoteFromKey(keyCode) {
        if (this.lastKeyCode == keyCode) {
            this.lastKeyCode = false;
            this.stop();
        }
    }

    updateVolume(value) {
        let volumeValue = Math.round(value) / 100;
        if (this.gainNode && !isNaN(volumeValue) && volumeValue >= 0 && volumeValue <= 1) {
            this.gainNode.gain.setValueAtTime(volumeValue, this.context.currentTime);
        }
    }
}